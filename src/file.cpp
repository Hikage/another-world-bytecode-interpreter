/**
 * \file file.cpp
 * \brief File handling implementation.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \version 2.1
 * \date 2021/05/05
 * \date 2021/05/18
 */

#include "file.hpp"

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

bool AnotherWorld::openFile (File &file, const std::string &filename,
                             const std::string &directory,
                             Endian fileEndianess) {
    namespace fs = boost::filesystem;
    namespace algo = boost::algorithm;

    if (file.is_open()) {file.close();}

    /// File name.
    std::string name = filename;
    /* First try to read file with name in lower-case. */
    algo::to_lower(name);
    /// Buffer to store the path to the file.
    fs::path buf (directory);
    buf /= name;
    file.open(buf.string(), fileEndianess);
    if (!file.is_open()) {
        /* Let’s try upper-case. */
        algo::to_upper(name);
        buf = directory;
        buf /= name;
        file.open(buf.string(), fileEndianess);
    }

    return file.is_open();
}
