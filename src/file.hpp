#ifndef __FILE_HPP__
#define __FILE_HPP__

/**
 * \file file.hpp
 * \brief Class to manage files into the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Meneboeuf, Christophe
 * \version 2.1
 * \date 2021/04/26
 * \date 2021/05/15
 */

#include <string>
#include <fstream>
#include <numeric>
#include <cstdint>
#include <cstddef>

#include "endian.hpp"

namespace AnotherWorld {
    /// \brief API to access assets files.
    class File {
        public:
            /// \brief Default constructor.
            File () {fileEndianess = nativeEndianess();}

            /**
             * \brief Opens a file.
             * \param filepath Path to the file to open.
             * \param fileEndianess What is endianess of the file to be opened.
             * \param mode Describes file access mode.
             */
            void open (const std::string &filepath, Endian _fileEndianess) {
                fileEndianess = _fileEndianess;
                file.open(filepath, std::ios_base::in | std::ios_base::binary);
            }

            /// \brief Closes the file.
            void close () {file.close();}

            /**
             * \brief To which endianess does comply the file?
             * \returns File endianess.
             */
            Endian endianess () const {return fileEndianess;}

            /**
             * \brief File openness status.
             * \returns Whether or not the file is open.
             *
             * Mimics standard file stream implementation.
             */
            bool is_open () const {return file.is_open();}

            /**
             * \brief File status.
             * \returns Whether or not the stream on the file is good.
             *
             * Mimics standard file stream implementation.
             */
            bool good () const {return file.good();}

            /**
             * \brief File status.
             * \returns Whether or not the stream reached the end of the file.
             *
             * Mimics standard file stream implementation.
             */
            bool oef () const {return file.eof();}

            /**
             * \brief File status.
             * \returns Whether or not the stream  on the file has failed.
             *
             * Mimics standard file stream implementation.
             */
            bool fail () const {return file.fail();}

            /**
             * \brief File status.
             * \returns Whether or not an unrecoverable error on the file
             * happened.
             *
             * Mimics standard file stream implementation.
             */
            bool bad () const {return file.bad();}

            /**
             * \brief Advancing for a given amount of byte inside the file.
             * \param off Required offset.
             */
            void seek (std::uint32_t off) {file.seekg(off);}

            /**
             * \brief Reads a value in the file.
             * \returns The value.
             */
            template <typename T> T read () {
                /// Temporarily stock the value.
                T temp;
                file.read(reinterpret_cast<char*>(&temp), sizeof(T));
                file2NativeInplace(temp, fileEndianess);
                return temp;
            }

            /**
             * \brief Reads a certain amount of bytes in the file.
             * \param s Space in which store bytes.
             * \param count How many bytes to read.
             */
            void read (char* s, std::size_t count) {file.read(s, count);}

        private:
            /// File descriptor.
            std::fstream file;

            /// File endianess.
            Endian fileEndianess;
    };

    /**
     * \brief Opens a file.
     * \param file Descriptor of the file to open.
     * \param filename Name of the file to open.
     * \param directory Directory in which the file is located.
     * \param fileEndianess What is endianess of the file to be opened.
     * \param mode Describes file access mode.
     * \returns Whether or not the file is open.
     */
    bool openFile (File &file, const std::string &filename,
                   const std::string &directory, Endian fileEndianess);
}

#endif
