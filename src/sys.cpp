/**
 * \file sysImplementation.cpp
 * \brief Definition of the system running the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 * \version 2.1
 * \date 2021/04/27
 * \date 2021/05/15
 */

#include <SDL.h>

#include <cstring>

#include "log.hpp"
#include "sys.hpp"

class SDLStub : public System {
   public:
    enum { SCREEN_W = 320, SCREEN_H = 200, SOUND_SAMPLE_RATE = 22050 };

    std::uint8_t DEFAULT_SCALE = 3;

    SDL_Surface *_screen = nullptr;
    SDL_Window *_window = nullptr;
    SDL_Renderer *_renderer = nullptr;
    std::uint8_t _scale = DEFAULT_SCALE;
    bool _fullscreen = false;

    SDLStub() = default;
    ~SDLStub() override = default;

    /**
     * \brief Indicates to what kind of endianess data files comply.
     * \returns Then endianess.
     */
    [[nodiscard]] AnotherWorld::Endian fileEndian() const override;

    /**
     * \brief Initialise system description.
     * \param title Game title.
     * \param fileEndian To which endian game data files comply.
     */
    void init(const char *title, AnotherWorld::Endian fileEndian) override;

    void destroy() override;
    void setPalette(const uint8_t *paletteBuffer) override;
    void updateDisplay(const uint8_t *src) override;
    void processEvents() override;
    void sleep(uint32_t duration) override;
    uint32_t getTimeStamp() override;
    void startAudio(AudioCallback callback, void *param) override;
    void stopAudio() override;
    uint32_t getOutputSampleRate() override;
    int addTimer(uint32_t delay, TimerCallback callback, void *param) override;
    void removeTimer(int timerId) override;
    void *createMutex() override;
    void destroyMutex(void *mutex) override;
    void lockMutex(void *mutex) override;
    void unlockMutex(void *mutex) override;

    void prepareGfxMode();
    void cleanupGfxMode();
    void switchGfxMode();

   private:
    /// Endianess to which data files comply.
    AnotherWorld::Endian _fileEndian{};
};

AnotherWorld::Endian SDLStub::fileEndian() const { return _fileEndian; }

void SDLStub::init(const char *, AnotherWorld::Endian fileEndian) {
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER);
    //	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY,
    // SDL_DEFAULT_REPEAT_INTERVAL);
    SDL_ShowCursor(SDL_DISABLE);

    SDL_ShowCursor(SDL_ENABLE);
    SDL_CaptureMouse(SDL_TRUE);

    memset(&input, 0, sizeof(input));
    _scale = DEFAULT_SCALE;
    prepareGfxMode();

    _fileEndian = fileEndian;
}

void SDLStub::destroy() {
    cleanupGfxMode();
    SDL_Quit();
}

static SDL_Color palette[NUM_COLORS];
void SDLStub::setPalette(const uint8_t *paletteBuffer) {
    // The incoming palette is in 565 format.
    for (auto &i : palette) {
        uint8_t c1 = *(paletteBuffer + 0);
        uint8_t c2 = *(paletteBuffer + 1);
        i.r = (((c1 & 0x0F) << 2) | ((c1 & 0x0F) >> 2)) << 2;  // r
        i.g = (((c2 & 0xF0) >> 2) | ((c2 & 0xF0) >> 6)) << 2;  // g
        i.b = (((c2 & 0x0F) >> 2) | ((c2 & 0x0F) << 2)) << 2;  // b
        i.a = 0xFF;
        paletteBuffer += 2;
    }
    SDL_SetPaletteColors(_screen->format->palette, palette, 0, NUM_COLORS);
}

void SDLStub::prepareGfxMode() {
    int w = SCREEN_W;
    int h = SCREEN_H;

    SDL_WindowFlags flag = SDL_WINDOW_SHOWN;

    if (_fullscreen) {
        flag = SDL_WINDOW_FULLSCREEN;
    }

    _window = SDL_CreateWindow("Another World", SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED, w * _scale, h * _scale,
                               flag);
    _renderer = SDL_CreateRenderer(_window, -1, 0);
    _screen = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 8, 0, 0, 0, 0);
    if (!_screen) {
        AnotherWorld::Logger::getLogger().error(
            "SDLStub::prepareGfxMode() unable to allocate _screen buffer");
    }
    // Upon resize during gameplay, the screen surface is re-created and a new
    // palette is allocated. This will result in an all-white surface palette
    // displaying a window full of white until a a palette is set by the VM. To
    // avoid this issue, we save the last palette locally and re-upload it each
    // time. On game start-up this is not requested.
    SDL_SetPaletteColors(_screen->format->palette, palette, 0, NUM_COLORS);
}

void SDLStub::updateDisplay(const uint8_t *src) {
    uint16_t height = SCREEN_H;
    auto *pixels = static_cast<uint8_t *>(_screen->pixels);

    // For each line
    while (height--) {
        // One byte gives us two pixels, we only need to iterate w/2 times.
        for (int i = 0; i < SCREEN_W / 2; ++i) {
            // Extract two palette indices from upper byte and lower byte.
            pixels[i * 2 + 0] = *(src + i) >> 4;
            pixels[i * 2 + 1] = *(src + i) & 0xF;
        }
        pixels += _screen->pitch;
        src += SCREEN_W / 2;
    }

    SDL_Texture *texture = SDL_CreateTextureFromSurface(_renderer, _screen);
    SDL_RenderCopy(_renderer, texture, nullptr, nullptr);
    SDL_RenderPresent(_renderer);
    SDL_DestroyTexture(texture);
}

void SDLStub::processEvents() {
    SDL_Event ev;
    while (SDL_PollEvent(&ev)) {
        const Uint8* keystate = SDL_GetKeyboardState(nullptr);
        switch (ev.type) {
            case SDL_QUIT:
                input.quit = true;
                break;
            case SDL_KEYUP:
                switch (ev.key.keysym.sym) {
                    case SDLK_LEFT:
                        input.dirMask &= ~PlayerInput::DIR_LEFT;
                        break;
                    case SDLK_RIGHT:
                        input.dirMask &= ~PlayerInput::DIR_RIGHT;
                        break;
                    case SDLK_UP:
                        input.dirMask &= ~PlayerInput::DIR_UP;
                        break;
                    case SDLK_DOWN:
                        input.dirMask &= ~PlayerInput::DIR_DOWN;
                        break;
                    case SDLK_SPACE:
                    case SDLK_RETURN :
                        input.button = false;
                        break;
                    case SDLK_q:
                    case SDLK_ESCAPE:
                        input.quit = true;
                        break;
                    case SDLK_s:
                        input.mute = !input.mute;
                        break;
                    case SDLK_PLUS:
                    case SDLK_KP_PLUS:
                        if (keystate[SDL_SCANCODE_LALT]) {
                            if (_scale < 4) {
                                ++_scale;
                                switchGfxMode();
                            }
                        } else {
                            input.volumeUP = true;
                        }
                        break;
                    case SDLK_MINUS:
                    case SDLK_KP_MINUS :
                        if (keystate[SDL_SCANCODE_LALT]) {
                            if (_scale > 1) {
                                --_scale;
                                switchGfxMode();
                            }
                        } else {
                            input.volumeDOWN = true;
                        }
                        break;
                    case SDLK_LALT :
                        if (keystate[SDL_SCANCODE_KP_PLUS]) {
                            if (_scale < 4) {
                                ++_scale;
                                switchGfxMode();
                            }
                        } else if (keystate[SDL_SCANCODE_KP_MINUS]) {
                            if (_scale > 1) {
                                --_scale;
                                switchGfxMode();
                            }
                        }
                        break;
                    case SDLK_f:
                        _fullscreen = !_fullscreen;
                        switchGfxMode();
                        break;
                }
                break;
            case SDL_KEYDOWN:
                input.lastChar = ev.key.keysym.sym;
                switch (ev.key.keysym.sym) {
                    case SDLK_LEFT:
                        input.dirMask |= PlayerInput::DIR_LEFT;
                        break;
                    case SDLK_RIGHT:
                        input.dirMask |= PlayerInput::DIR_RIGHT;
                        break;
                    case SDLK_UP:
                        input.dirMask |= PlayerInput::DIR_UP;
                        break;
                    case SDLK_DOWN:
                        input.dirMask |= PlayerInput::DIR_DOWN;
                        break;
                    case SDLK_SPACE:
                    case SDLK_RETURN:
                        input.button = true;
                        break;
                    case SDLK_c:
                        input.code = true;
                        break;
                    case SDLK_p:
                        input.pause = true;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
}

void SDLStub::sleep(uint32_t duration) { SDL_Delay(duration); }

uint32_t SDLStub::getTimeStamp() { return SDL_GetTicks(); }

void SDLStub::startAudio(AudioCallback callback, void *param) {
    SDL_AudioSpec desired;
    memset(&desired, 0, sizeof(desired));

    desired.freq = SOUND_SAMPLE_RATE;
    desired.format = AUDIO_U8;
    desired.channels = 1;
    desired.samples = 2048;
    desired.callback = callback;
    desired.userdata = param;
    if (SDL_OpenAudio(&desired, nullptr) == 0) {
        SDL_PauseAudio(0);
    } else {
        AnotherWorld::Logger::getLogger().error(
            "SDLStub::startAudio() unable to open sound device");
    }
}

void SDLStub::stopAudio() { SDL_CloseAudio(); }

uint32_t SDLStub::getOutputSampleRate() { return SOUND_SAMPLE_RATE; }

int SDLStub::addTimer(uint32_t delay, TimerCallback callback, void *param) {
    return SDL_AddTimer(delay, (SDL_TimerCallback)callback, param);
}

void SDLStub::removeTimer(int timerId) { SDL_RemoveTimer(timerId); }

void *SDLStub::createMutex() { return SDL_CreateMutex(); }

void SDLStub::destroyMutex(void *mutex) {
    SDL_DestroyMutex((SDL_mutex *)mutex);
}

void SDLStub::lockMutex(void *mutex) { SDL_mutexP((SDL_mutex *)mutex); }

void SDLStub::unlockMutex(void *mutex) { SDL_mutexV((SDL_mutex *)mutex); }

void SDLStub::cleanupGfxMode() {
    if (_screen) {
        SDL_FreeSurface(_screen);
        _screen = nullptr;
    }

    if (_window) {
        SDL_DestroyWindow(_window);
        _window = nullptr;
    }

    if (_screen) {
        SDL_FreeSurface(_screen);
        _screen = nullptr;
    }
}

void SDLStub::switchGfxMode() {
    cleanupGfxMode();
    prepareGfxMode();
}

/* System description. */
SDLStub sysImplementation;
/* Pointer to access system description. */
System *stub = &sysImplementation;
