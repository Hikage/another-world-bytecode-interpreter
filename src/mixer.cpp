/**
 * \file mixer.cpp
 * \brief Implementation for mixing audio channels.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \version 2.0
 * \date 2021/05/19
 * \date 2021/05/19
 */

#include <cassert>
#include <cstring>

#include "mixer.hpp"

#include "sys.hpp"
#include "log.hpp"
#include "ilog.hpp"

namespace AnotherWorld {
    /**
     * \brief Addition with boundary control.
     * \param a First value to add.
     * \param b Second value to add.
     * \returns The total.
     */
    static std::int8_t addclamp (int a, int b) {
        /// Total without boundary control.
        int add = a + b;
        if (add < -128) {
            add = -128;
        } else if (add > 127) {
            add = 127;
        }
        return static_cast<std::int8_t>(add);
    }

    void Mixer::init () {
        std::memset(channels, 0, sizeof(channels));
        mutex = sys->createMutex();
        sys->startAudio(Mixer::mixCallback, this);
    }

    void Mixer::free() {
        stopAll();
        sys->stopAudio();
        sys->destroyMutex(mutex);
    }

    void Mixer::playChannel (std::uint8_t channel, const MixerChunk *mc,
                             std::uint16_t freq, std::uint8_t volume) {
        Logger::getLogger().debug(ILogger::DBG_SND,
                                  "Mixer::playChannel(%d, %d, %d)", channel,
                                  freq, volume);
        assert(channel < audioChannels);

        /// The mutex is acquired in the constructor
        auto _mutex = MutexStack (sys, mutex);
        /// Reference to the mixer channels.
        MixerChannel* ch = &channels[channel];
        ch->active = true;
        ch->requestedVolume = volume;
        ch->volume = volume * volumeFactor / 100;
        ch->chunk = *mc;
        ch->chunkPos = 0;
        ch->chunkInc = (freq << 8) / sys->getOutputSampleRate();

        /* At the end of the scope the MutexStack destructor is called and the
         * mutex is released. */
    }

    void Mixer::stopChannel (std::uint8_t channel) {
        Logger::getLogger().debug(ILogger::DBG_SND, "Mixer::stopChannel(%d)",
                                  channel);
        assert(channel < audioChannels);
        /// Reference to current mutex.
        auto _mutex = MutexStack (sys, mutex);
        channels[channel].active = false;
    }

    void Mixer::setChannelVolume (std::uint8_t channel, std::uint8_t volume) {
        Logger::getLogger().debug(ILogger::DBG_SND,
                                  "Mixer::setChannelVolume(%d, %d)", channel,
                                  volume);
        assert(channel < audioChannels);
        /// Reference to current mutex.
        auto _mutex = MutexStack (sys, mutex);
        channels[channel].requestedVolume = volume;
        channels[channel].volume = volume * volumeFactor / 100;
    }

    void Mixer::stopAll () {
        Logger::getLogger().debug(ILogger::DBG_SND, "Mixer::stopAll()");
        /// Reference to current mutex.
        auto _mutex = MutexStack (sys, mutex);
        for (auto &channel : channels) {
            channel.active = false;
        }
    }

    void Mixer::mix (std::int8_t* buf, int len) {
        /// Pointer to the buffer.
        std::int8_t* pBuf;

        /// Reference to current mutex.
        auto _mutex = MutexStack(sys, mutex);

        /* Clear the buffer since nothing guaranty we are receiving clean
         * memory. */
        std::memset(buf, 0, len);

        if (!sys->input.mute) {
            for (std::uint8_t i = 0; i < audioChannels; ++i) {
                /// Reference to the current audio channel.
                MixerChannel* ch = &channels[i];
                if (!ch->active) continue;

                pBuf = buf;
                for (int j = 0; j < len; ++j, ++pBuf) {
                    /// To be completed ...
                    std::uint16_t p1;
                    /// To be completed ...
                    std::uint16_t p2;
                    /// To be completed ...
                    std::uint16_t ilc = (ch->chunkPos & 0xFF);
                    p1 = ch->chunkPos >> 8;
                    ch->chunkPos += ch->chunkInc;

                    if (ch->chunk.loopLen != 0) {
                        if (p1 == ch->chunk.loopPos + ch->chunk.loopLen - 1) {
                            Logger::getLogger().debug(ILogger::DBG_SND,
                                                      "Looping sample on channel %d",
                                                      i);
                            ch->chunkPos = p2 = ch->chunk.loopPos;
                        } else {
                            p2 = p1 + 1;
                        }
                    } else {
                        if (p1 == ch->chunk.len - 1) {
                            Logger::getLogger().debug(ILogger::DBG_SND,
                                                      "Stopping sample on channel %d",
                                                      i);
                            ch->active = false;
                            break;
                        } else {
                            p2 = p1 + 1;
                        }
                    }
                    /* Interpolate. */
                    /// To be completed ...
                    std::int8_t b1 =
                        *reinterpret_cast<std::int8_t*>(const_cast<std::uint8_t*>(ch->chunk.data + p1));
                    /// To be completed ...
                    std::int8_t b2 =
                        *reinterpret_cast<std::int8_t*>(const_cast<std::uint8_t*>(ch->chunk.data + p2));
                    /// To be completed ...
                    std::int8_t b =
                        static_cast<std::int8_t>((b1 * (0xFF - ilc) + b2 * ilc) >> 8);

                    /* Set volume and clamp. */
                    *pBuf = addclamp(*pBuf, (int) b * ch->volume / 0x40);  // 0x40=64
                }
            }
        }
        /* Convert signed 8-bit PCM to unsigned 8-bit PCM. The
         * current version of SDL hangs when using signed 8-bit
         * PCM in combination with the PulseAudio driver. */
        pBuf = buf;
        for (int j = 0; j < len; ++j, ++pBuf) {
            *reinterpret_cast<uint8_t *>(pBuf) = (*pBuf + 128);
        }

        if (sys->input.volumeUP) {
            volumeFactor = std::min(volumeFactor + 10, 100);
            Logger::getLogger().log(INFORMATION,
                                    "Sound volume raised to : %d",
                                    volumeFactor);
        } else if (sys->input.volumeDOWN) {
            volumeFactor = std::max(volumeFactor - 10, 0);
            Logger::getLogger().log(INFORMATION,
                                    "Sound volume lowered to : %d",
                                    volumeFactor);
        }

        if (sys->input.volumeDOWN || sys->input.volumeUP) {
            for (uint8_t i = 0; i < audioChannels; ++i) {
                /// Reference to the current audio channel.
                MixerChannel* ch = &channels[i];
                ch->volume = ch->requestedVolume * volumeFactor / 100;
            }
            sys->input.volumeDOWN = false;
            sys->input.volumeUP = false;
        }
    }
}
