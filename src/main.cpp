/**
 * \file main.cpp
 * \brief A bytecode interpreter for Éric Chahi’s classical game Another World.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Willll
 * \version 2.1
 * \date 2021/04/24
 * \date 2021/05/13
 */

/**
 * \mainpage
 *
 * Raw is a reimplementation of the virtual machine for Another World. This
 * program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License  as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version. See file `LICENSE.txt` or go to:
 *
 * <https://choosealicense.com/licenses/gpl-2.0/>
 *
 * Command line:
 *
 *     raw [Options]
 *
 * Supported options:
 *
 *     -h [ --help ]              Display this help message.
 *     -v [ --version ]           Display program version.
 *     -d [ --datapath ] arg (=.) Path to game data.
 *     -c [ --debugConsole ]      Enable logs through the standard output.
 *     -f [ --debugFile ] arg     Enable logs into a file.
 *     -s [ --debugSyslog ] arg   Enable logs through Syslog.
 *     -l [ --level ] arg         Log level filter
 *
 * The available log levels are:
 *
 * - debug
 * - information
 * - warning
 * - error
 *
 * The higher level also trigger on the lower levels. For instance, if you ask
 * for "warning" error, you will also get "error" level. Default is "error".
 *
 * Here are the various in game hotkeys:
 *
 * - \<Arrow Keys\>: allow you to move Lester
 * - \<Enter\>/\<Space\>: allow you run/shoot with your gun
 * - \<C\>: allow to enter a code to jump at a specific level
 * - \<P\>: pause the game
 * - \<Esc\>/\<Q\>: exit the game
 * - \<Alt\> + \<+\> and \<Alt\> + \<-\>: change window scale factor
 * - \<F\>: toggle full-screen mode
 * - \<S\>: toggle sound off and on
 * - \<+\> and \<-\>: increase and decrease sound level
 *
 *
 * \todo This code uses way to many raw pointers, this has to be fixed.
 *
 * \todo There is some concern with the timer to be fixed.
 *
 * \todo Add support of every version, using work from RawGL.
 *
 * \todo Add OpenGL support, using work from RawGL.
 *
 * \todo This code uses a system abstraction which serve no purpose and should
 * be removed.
 *
 * \todo Add support for gamepads and joysticks.
 *
 * \todo Use system thread to take advantage of actual pre-emptive
 * multi-threading.
 *
 * \todo Add full screen option.
 *
 * \todo Add functionality to set sound volume.
 *
 * \todo Fullscreen needs some fixing.
 */

#include <boost/program_options.hpp>
#include <config.hpp>
#include <iostream>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <exception>
#include <cstdint>

#include "engine.hpp"
#include "sys.hpp"
#include "log.hpp"
#include "ilog.hpp"

/**
 * \brief Main function of the program.
 * \param argc Count of arguments transmitted to the program.
 * \param argv Values of arguments transmitted to the program.
 * \returns 0 if everything went well,
 *     -1 if the given data path does not exists,
 *     -2 if the given data path does not lead to a directory,
 *     -3 it the command line is incorrect,
 *     -4 if a standard exception occurred,
 *     -5 if an unknown exception occurred.
 */
int main(int argc, char** argv) {
    namespace po = boost::program_options;
    namespace fs = boost::filesystem;
    namespace algo = boost::algorithm;
    namespace aw = AnotherWorld;

    /// Default path where to find game data.
    const char* defaultDataPath = ".";
    /// Default severity logging level.
    const aw::SeverityLevels defaultLogLevel = aw::SeverityLevels::DEBUG;
    /// Default name of the log file.
    const char* defaultFileLoggerFile = "debug.log";
    /// Default IP for logging system.
    const char* defaultsyslogLoggerIp = "localhost:514";
    /// Default port for logging system.
    const std::uint16_t defaultsyslogLoggerPort = 514;

    /* -- Reading the command line. -- */

    /// Path to game data.
    std::string dataPath = defaultDataPath;
    /// Console Logger enable flag
    bool isConsoleLoggerEnable = false;
    /// File Logger file
    std::string fileLoggerfile = defaultFileLoggerFile;
    /// Syslog Logger IP
    std::string syslogLoggerIp = defaultsyslogLoggerIp;
    /// Logs level
    std::string logLevel = "";

    try {
        /// Declaring supported options.
        po::options_description desc("Supported options");
        desc.add_options()
            ("help,h", "Display this help message.")
            ("version,v", "Display program version.")
            ("datapath,d",
             po::value<std::string>(&dataPath)->default_value(defaultDataPath),
             "Path to game data.")
            ("debugConsole,c",
             po::bool_switch(&isConsoleLoggerEnable),
             "Enable logs through the standard output.")
            ("debugFile,f",
             po::value<std::string>(&fileLoggerfile),
             "Enable logs into a file.")
            ("debugSyslog,s", po::value<std::string>(&syslogLoggerIp),
             "Enable logs through Syslog.")
            ("level,l", po::value<std::string>(&logLevel),
             "Log level filter. The available log levels are:\n\n- debug\n- information\n- warning\n- error\n\nThe higher level also trigger on the lower levels. For instance, if you ask for “warning” error, you will also get “error” level. Default is “error”.");
        /// Command line.
        po::options_description cmd;
        cmd.add(desc);

        /// Options map.
        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(cmd).run(), vm);
        po::notify(vm);

        /// Indicates whether or not the program should be stopped.
        bool stop = false;

        /* Check if help message should be displayed. */
        if (vm.count("help")) {
            std::cout << "Raw is a reimplementation of the virtual machine "
                      << "for Another World.\n\n"
                      << "Command: \n\n"
                      << '\t' << argv[0] << " [Options]\n\n"
                      << desc << '\n';
            stop = true;
        }

        /* Check if program version should be displayed. */
        if (vm.count("version")) {
            std::cout << argv[0] << " version " << Configuration::versionMajor
                      << '.' << Configuration::versionMinor << '.'
                      << Configuration::patchVersion << '\n';
            stop = true;
        }

        if (stop) return 0;

        if (!fs::exists(dataPath)) {
            std::cerr << "Data path incorrect: “" << dataPath
                      << "” does not exist.\n";
            return -1;
        }
        if (!fs::is_directory(dataPath)) {
            std::cerr << "Data path incorrect: “" << dataPath
                      << "” does not lead to a directory.\n";
            return -2;
        }

        /* Format input log level */
        algo::trim(logLevel);
        algo::to_lower(logLevel);

        /* -- Logs Initialization -- */

        aw::LoggerConfiguration configuration (isConsoleLoggerEnable,
                                               (vm.count("debugFile") >= 1),
                                               (vm.count("debugSyslog") >= 1),
                                               !logLevel.empty()?
                                                    aw::to_SeverityLevel(logLevel):
                                                    defaultLogLevel,
                                               fileLoggerfile,
                                               [&](const std::string &ip){
                                                   /// Position of the port.
                                                   auto end = ip.find(':');
                                                   return (end != std::string::npos)?
                                                    ip.substr(0, end): ip;
                                               }(syslogLoggerIp),
                                               [&](const std::string &ip){
                                                   /// Position of the port.
                                                   auto end = ip.find(':');
                                                   // That's a hellish oneliner !
                                                   return (end != std::string::npos)?
                                                       static_cast<std::uint16_t>(std::stoi(ip.substr(end+1, ip.size()))):
                                                       defaultsyslogLoggerPort;
                                               }(syslogLoggerIp));

        aw::Logger::loggerFactory(configuration);

        /* -- Launch game. -- */

        /// System description.
        extern System *stub;

        /// Game engine instantiation.
        aw::Engine e (stub, dataPath);

        return e.run();
    }
    catch (po::error &e) {
        std::cerr << e.what() << '\n';
        return -3;
    }
    catch (std::exception &e) {
        std::cerr << e.what() << '\n';
        return -4;
    }
    catch (...) {
        std::cerr << "An unknown exception occurred.\n";
        return -5;
    }
}
