/**
 * \file logBoost.cpp
 * \brief class to implement the logs via Boost log library
 * \author Willll
 * \version 0.1
 * \date 2021/05/11
 * \date 2021/05/11
 */

#include "logBoost.hpp"

#include <cstdarg>

#include <boost/log/common.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/attributes/timer.hpp>
#include <boost/log/sinks/syslog_backend.hpp>

namespace AnotherWorld {
    namespace logging = boost::log;
    namespace keywords = boost::log::keywords;
    namespace src = boost::log::sources;
    namespace attrs = boost::log::attributes;
    namespace expr = boost::log::expressions;
    namespace sinks = boost::log::sinks;

    using boost::shared_ptr;

    /// \brief boost Logger object instantiation
    BOOST_LOG_INLINE_GLOBAL_LOGGER_DEFAULT(raw_logger,
                                           src::severity_logger<SeverityLevels>)

    LogBoost::LogBoost (bool isSet): ILogger (isSet) {}

    void LogBoost::setupSyslog(const LoggerConfiguration &configuration) {

        /// Log sending sink.
        shared_ptr<sinks::synchronous_sink<sinks::syslog_backend> > sink(
                new sinks::synchronous_sink<sinks::syslog_backend>());

        /// We'll have to map our custom levels to the syslog levels
        sinks::syslog::custom_severity_mapping<SeverityLevels> mapping("Severity");
        mapping[DEBUG] = sinks::syslog::debug;
        mapping[INFORMATION] = sinks::syslog::info;
        mapping[WARNING] = sinks::syslog::warning;
        mapping[ERROR] = sinks::syslog::critical;

        sink->locked_backend()->set_severity_mapper(mapping);

#if !defined(BOOST_LOG_NO_ASIO)
        // Set the remote address to sent syslog messages to
        sink->locked_backend()->set_target_address(configuration.getLogIP(),
                                                   configuration.getLogPort());
#endif

        sink->set_filter(expr::attr<SeverityLevels>("Severity") >= configuration.getSeverityLevel());

        sink->set_formatter(expr::stream
                                    << "["
                                    << expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%H:%M:%S.%f")
                                    << "] <"
                                    << expr::attr<SeverityLevels>("Severity")
                                    << "> " << expr::message << '\n');

        // Add the sink to the core
        logging::core::get()->add_sink(sink);
    }

    void LogBoost::setup (const LoggerConfiguration &configuration) {
        /// Logging level to be outputted on the console.
        SeverityLevels consoleSeverityLevel = configuration.getSeverityLevel();
        /// Logging level to be outputted on a file.
        SeverityLevels fileSeverityLevel = configuration.getSeverityLevel();

        if (!configuration.isConsoleEnable()) {
            consoleSeverityLevel = DISABLE;
        }

        if (!configuration.isFileEnable()) {
            fileSeverityLevel = DISABLE;
        }

        logging::add_console_log(
                /// Log sender.
                std::clog,
                keywords::filter =
                    expr::attr<SeverityLevels>("Severity") >= consoleSeverityLevel,
                keywords::format = expr::stream
                        << "[" << expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%H:%M:%S.%f")
                        << "] <" << expr::attr<SeverityLevels>("Severity")
                        << "> " << expr::message);

        if (configuration.isFileEnable()) {
            logging::add_file_log(
                    configuration.getLogFilename(),
                    keywords::filter =
                        expr::attr<SeverityLevels>("Severity") >= fileSeverityLevel,
                    keywords::format = expr::stream
                            << "[" << expr::format_date_time<boost::posix_time::ptime>("TimeStamp",
                                                                                       "%Y-%m-%d, %H:%M:%S.%f")
                            << "] <" << expr::attr<SeverityLevels>("Severity")
                            << "> " << expr::message);
        }

        if (configuration.isSyslogEnable()) {
            setupSyslog(configuration);
        }

        logging::add_common_attributes();

        BOOST_LOG_FUNCTION();
    }

    void LogBoost::log (SeverityLevels slvl, const char* format, ...) {
        if (isSet) {
            /// Command line parameters.
            std::va_list va;
            va_start(va, format);
            log(slvl, format, va);
            va_end(va);
        }
    }

    void LogBoost::debug (ILogger::DebugSource cm, const char* format, ...) {
        if (isSet && cm) {
            /// Command line parameters.
            std::va_list va;
            va_start(va, format);
            log(DEBUG, format, va);
            va_end(va);
        }
    }

    void LogBoost::warning (const char* format, ...) {
        if (isSet) {
            /// Command line parameters.
            std::va_list va;
            va_start(va, format);
            log(WARNING, format, va);
            va_end(va);
        }
    }

    void LogBoost::error (const char *format, ...) {
        if (isSet) {
            /// Command line parameters.
            std::va_list va;
            va_start(va, format);
            log(ERROR, format, va);
            va_end(va);
        }
    }

    void LogBoost::log (SeverityLevels slvl, const char* format,
                        std::va_list va) {
        if (isSet) {
            /// Buffer to stock error message.
            char buf[1024];
            std::vsprintf(buf, format, va);
            BOOST_LOG_SEV(raw_logger::get(), slvl) << buf;
        }
    }
}
