#ifndef __MIXER_HPP__
#define __MIXER_HPP__

/**
 * \file mixer.hpp
 * \brief Definition for mixing sounds in the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \version 2.0
 * \date 2021/05/19
 * \date 2021/05/19
 */

#include <cstdint>

#include "intern.hpp"

// Forward declaration.
class System;

namespace AnotherWorld {
    /// \brief Describe an audio element.
    struct MixerChunk {
        /// \brief Pointer to where the element is stored in memory.
        const std::uint8_t* data;

        /// \brief Element duration.
        std::uint16_t len;

        /// \brief Where does the loop start.
        std::uint16_t loopPos;

        /// \brief Loop duration.
        std::uint16_t loopLen;
    };

    /// \brief Audio mixer controller.
    struct MixerChannel {
        /// \brief Whether or not the channel is active.
        std::uint8_t active;

        /// \brief Sound volume requested by the user.
        std::uint8_t requestedVolume;

        /// \brief Actual sound volume.
        std::uint8_t volume;

        /// \brief Element descriptor.
        MixerChunk chunk;

        /// \brief Element position in memory.
        std::uint32_t chunkPos;

        /// \brief To be completed ...
        std::uint32_t chunkInc;
    };

    /// \brief Number of audio channels for the game.
    const std::uint8_t audioChannels = 4;

    /// \brief Describing an audio mixer for the game.
    class Mixer {
        public :
            /**
             * \brief Constructor.
             * \param stub System descriptor.
             */
            explicit Mixer (System* stub): mutex (nullptr), sys (stub),
                                           volumeFactor(75) {}

            /**
             * \brief Initiating the mixer.
             * \todo This probably should be done in the constructor.
             */
            void init ();

            /**
             * \brief Freeing memory.
             * \todo This probably should be done in the destructor.
             */
            void free ();

            /**
             * \brief Playing sound on a given audio channel.
             * \param channel The channel to be played.
             * \param mc Element descriptor.
             * \param freq Sound frequency.
             * \param volume Sound volume.
             */
            void playChannel (std::uint8_t channel, const MixerChunk* mc,
                              std::uint16_t freq, std::uint8_t volume);

            /**
             * \brief Stop a channel for playing sound.
             * \param channel Which channel to stop.
             */
            void stopChannel (std::uint8_t channel);

            /**
             * \brief Define volume for a given channel.
             * \param channel Which channel to deal with.
             * \param volume Sound volume to set.
             */
            void setChannelVolume (std::uint8_t channel, std::uint8_t volume);

            /// \brief Stop sound in all channels.
            void stopAll ();

            /**
             * \brief Mixing sounds.
             * \param buf Sound buffer.
             * \param len Sound length.
             *
             * This is SDL callback. Called in order to populate the buf with len
             * bytes. The mixer iterates through all active channels and combine
             * all sounds.
             *
             * Since there is no way to know when SDL will ask for a buffer fill,
             * we need to synchronize with a mutex so the channels remain stable
             * during the execution of this method.
             */
            void mix (std::int8_t* buf, int len);

            /**
             * \brief Callback for the mixer.
             * \param param Mixer parameters.
             * \param buf Sound buffer.
             * \param len Sound length.
             */
            static void mixCallback (void* param, std::uint8_t* buf, int len) {
                (reinterpret_cast<Mixer*>(param))->mix(reinterpret_cast<int8_t*>(buf), len);
            }

        private:
            /**
             * \brief Mutex for synchronisation with the virtual machine.
             *
             * Since the virtual machine and SDL are running simultaneously in two
             * different threads any read or write to an elements of the sound
             * channels MUST be synchronized with a mutex.
             */
            void* mutex;

            /// \brief System descriptor.
            System* sys;

            /// \brief Pourcentage of sound volume with regards to the maximum.
            std::uint8_t volumeFactor;

            /// \brief Audio channels for the game.
            MixerChannel channels [audioChannels] {};
    };
}

#endif
