#ifndef __SYS_HPP__
#define __SYS_HPP__

/**
 * \file sys.hpp
 * \brief Description of the system on which the game is running.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author willl
 * \version 2.0
 * \date 2021/05/11
 * \date 2021/05/11
 */

#include "endian.hpp"

#define NUM_COLORS 16
#define BYTE_PER_PIXEL 3

struct PlayerInput {
    enum {
        DIR_LEFT = 1 << 0,
        DIR_RIGHT = 1 << 1,
        DIR_UP = 1 << 2,
        DIR_DOWN = 1 << 3
    };

    uint8_t dirMask;
    bool button;
    bool code;
    bool pause;
    bool quit;
    bool mute;
    bool volumeUP;
    bool volumeDOWN;
    char lastChar;
};

/*
        System is an abstract class so any find of system can be plugged
   underneath.
*/
class System {
    public:
        typedef void (*AudioCallback)(void *param, uint8_t *stream, int len);
        typedef uint32_t (*TimerCallback)(uint32_t delay, void *param);

        PlayerInput input;

        virtual ~System() {}

        /**
         * \brief Indicates to what kind of endianess data files comply.
         * \returns Then endianess.
         */
        virtual AnotherWorld::Endian fileEndian () const = 0;

        /**
         * \brief Initialise system description.
         * \param title Game title.
         * \param fileEndian To which endian game data files comply.
         */
        virtual void init (const char *title, AnotherWorld::Endian fileEndian) = 0;

        virtual void destroy() = 0;

        virtual void setPalette(const uint8_t *buf) = 0;
        virtual void updateDisplay(const uint8_t *buf) = 0;

        virtual void processEvents() = 0;
        virtual void sleep(uint32_t duration) = 0;
        virtual uint32_t getTimeStamp() = 0;

        virtual void startAudio(AudioCallback callback, void *param) = 0;
        virtual void stopAudio() = 0;
        virtual uint32_t getOutputSampleRate() = 0;

        virtual int addTimer(uint32_t delay, TimerCallback callback,
                             void *param) = 0;
        virtual void removeTimer(int timerId) = 0;

        virtual void *createMutex() = 0;
        virtual void destroyMutex(void *mutex) = 0;
        virtual void lockMutex(void *mutex) = 0;
        virtual void unlockMutex(void *mutex) = 0;
};

struct MutexStack {
    System *sys;
    void *_mutex;

    MutexStack(System *stub, void *mutex) : sys(stub), _mutex(mutex) {
        sys->lockMutex(_mutex);
    }
    ~MutexStack() { sys->unlockMutex(_mutex); }
};

#endif
