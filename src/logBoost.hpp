#ifndef __LOGBOOST_HPP__
#define __LOGBOOST_HPP__

/**
 * \file logBoost.hpp
 * \brief class to implement the logs via Boost log library
 * \author Willll
 * \version 0.1
 * \date 2021/05/11
 * \date 2021/05/15
 */

#include <cstdarg>

#include "ilog.hpp"

namespace AnotherWorld {
    /// \brief Class for Boost.log.
    class LogBoost : public ILogger {
        public :
            /// \brief Default constructor disabled
            LogBoost() = delete;

            /**
             * \brief Constructor
             * \param isSet Whether or not enable logging.
             */
            explicit LogBoost (bool isSet);

            /// \brief destructor
            virtual ~LogBoost () {}

            /**
             * \brief Sending a log message.
             * \param slvl Message severity level.
             * \param format Message format describer.
             */
            void log (SeverityLevels slvl, const char* format, ...) override;

            /**
             * \brief Sending a debug message.
             * \param cm Code for message to generate.
             * \param format Message format describer.
             */
            void debug (ILogger::DebugSource cm,
                        const char* format, ...) override;

            /**
             * \brief Sending a warning message.
             * \param format Message format describer.
             */
            void warning (const char* format, ...) override;

            /**
             * \brief Sending an error message.
             * \param format Message format describer.
             */
            void error (const char* format, ...) override;

            /**
             * \brief setup logger
             * \param configuration logger configuration object.
             */
            void setup (const LoggerConfiguration &configuration) override;

        private :
            /**
             * \brief setup syslog logger
             * \param configuration logger configuration object.
             */
            void setupSyslog (const LoggerConfiguration &configuration);

            /**
            * \brief Base for every log message.
            * \param slvl Security level for logging.
            * \param format Message format describer.
            * \param va Command line arguments.
            */
            void log (SeverityLevels slvl, const char *format, std::va_list va);
    };
}

#endif
