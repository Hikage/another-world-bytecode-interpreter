/**
 * \file engine.cpp
 * \brief Game engine implementation.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 * \version 2.1
 * \date 2021/04/26
 * \date 2021/05/09
 */

#include "engine.hpp"

#include "endian.hpp"

AnotherWorld::Engine::Engine (System *paramSys, const std::string &_dataDir):
                        dataDir (_dataDir),
                        sys (paramSys),
                        mixer (sys),
                        res (&video, dataDir.c_str()),
                        video (&res, sys),
                        player (&mixer, &res, sys),
                        vm (&mixer, &res, &player, &video, sys) {
    sys->init("Out Of This World", bigEndian);

    video.init();

    res.readEntries();

    vm.init();

    mixer.init();

    player.init();

    /**
     * At which part of the game to start. GAME_PART1 leads to
     * protection screen, while GAME_PART2 leads directly to game
     * introduction, bypassing the protection. To jump to further
     * part of the game, you also must set the game state,
     * otherwise it will crash.
     */
#ifdef BYPASS_PROTECTION
    Game::PartIdentifier part = Game::PART2;
#else
    Game::PartIdentifier part = Game::PART1;
#endif
    vm.initForPart(part);
}

AnotherWorld::Engine::~Engine () {
    player.free();
    mixer.free();
    sys->destroy();
    sys = nullptr;
}

int AnotherWorld::Engine::run () {
    while (!sys->input.quit) {
        vm.checkThreadRequests();

        vm.inp_updatePlayer();

        vm.hostFrame();
    }

    return 0;
}
