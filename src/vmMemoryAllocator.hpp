#ifndef __VMMEMORYALLOCATOR_HPP__
#define __VMMEMORYALLOCATOR_HPP__

/**
 * \file vmMemoryAllocator.hpp
 * \brief Implements the Memory Allocator used by the Virtual Machine.
 * \author Glaize, Sylvain
 * \version 1.0
 * \date 2021/05/09
 * \date 2021/05/09
 */

#include <array>
#include <cassert>
#include <cstdint>

namespace AnotherWorld {
    /** \brief The allocator for the Virtual Machine memory.
     *
     * The allocator reserves memory that can be allocated by the VM.
     * The allocation is forward only, and can be reset either fully or
     * at a marked position.
     *
     * The allocator also reserves space reserved for RT_POLY_ANIM.
     *
     * The allocation interface still matches the original extraction, with
     * a manual pointer get/reserve scheme. Further work could be done to
     * mimic a more conventional 'alloc' scheme.
     *
     * Currently, verifying the space left is still the caller's
     * responsibility.
     */
    class VMMemoryAllocator {
        public:
            /**
             * \brief Constructs the allocator. Memory is reserved and ready to be
             * allocated by the VM
             */
            VMMemoryAllocator ();

            /// \brief Allocator is freed, it's reserved memory also.
            ~VMMemoryAllocator() = default;

            /// \brief Non copyable class.
            VMMemoryAllocator(const VMMemoryAllocator &) = delete;

            /// \brief Non copyable class.
            VMMemoryAllocator &operator = (const VMMemoryAllocator &) = delete;

            /// \brief Non movable class.
            VMMemoryAllocator (VMMemoryAllocator &&) = delete;

            /// \brief Non movable class.
            VMMemoryAllocator &operator = (VMMemoryAllocator &&) = delete;

            /**
             * \brief Gets the pointer to the space reserved for RT_POLY_ANIM.
             * \returns The pointer to the space reserved for RT_POLY_ANIM.
             */
            [[nodiscard]] std::uint8_t* getCurrentVideoPointer () const {
                return currentVideoPointer;
            }

            /**
             * \brief Gets the pointer to the next free space for script
             * allocation.
             * \returns The pointer to the next free space.
             */
            [[nodiscard]] std::uint8_t* getCurrentScriptPointer () const {
                return currentScriptPointer;
            }

            /**
             * \brief Gets the space left for script memory.
             * \returns Number of space left in bytes.
             */
            std::uint32_t getFreeScriptMemory () {
                return currentVideoPointer - currentScriptPointer;
            }

            /**
             * \brief Allocates space for script in the memory. Verifying if
             * there's enough space left is the caller's responsibility.
             */
            void reserveMemoryForScript (std::uint32_t size) {
                currentScriptPointer += size;
                assert(currentScriptPointer < currentVideoPointer);
            }

            /**
             * \brief Frees the allocated memory from the marked point (by
             * markScriptMemory() ) up to the latest allocation.
             */
            void freeResourceMemory () {
                currentScriptPointer = markedScriptPointer;
            }

            /**
             * \brief Frees the whole script memory, regardless of the marked
             * point set by markScriptMemory().
             */
            void freeAllScriptMemory () {
                currentScriptPointer = memory.data();
            }

            /**
             * \brief Marks the allocator. When using `freeResourceMemory()`,
             * the memory allocated before the mark will be preserved.
             */
            void markScriptMemory () {
                markedScriptPointer = currentScriptPointer;
            }

            /**
             * \brief Gets the offset in bytes of the given pointer in the
             * allocated memory. This function is used by the debug log only.
             * \param loadDestination The given pointer. If it was not
             * allocated by this allocator, the result is undefined.
             * \returns The offset between the passed pointer and the start of
             * the reserved space.
             */
            std::uint32_t getOffsetInMemory (std::uint8_t* loadDestination)
                    const {
                return loadDestination - memory.data();
            }

        private:
            enum {
                /**
                 * \brief 600kb total memory consumed (not taking into account
                 * stack and staticheap).
                 */
                MEM_BLOCK_SIZE = 600 * 1024,
            };
            enum {
                /// \brief Additional memory for vidBack and vidCur.
                VIDEO_BLOCK_SIZE = 0x800 * 16,
            };

            /// Reserved memory
            std::array<std::uint8_t, MEM_BLOCK_SIZE> memory {};

            /// The pointer to the marked space in the reserved memory.
            std::uint8_t* markedScriptPointer;

            /// The pointer to the next yet unallocated memory.
            std::uint8_t* currentScriptPointer;

            /// The pointer at start of the video reserved memory.
            std::uint8_t* currentVideoPointer;
    };
}

#endif
