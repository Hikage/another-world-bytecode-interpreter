/**
 * \file logLegacy.cpp
 * \brief class to implement the logs as they were originally designed
 * \author Willll
 * \version 0.1
 * \date 2021/05/11
 * \date 2021/05/11
 */

#include "logLegacy.hpp"

#include <cstdarg>
#include <cstdio>

AnotherWorld::LogLegacy::LogLegacy (bool isSet):
        ILogger(isSet), severityLevel(SeverityLevels::DISABLE) {}

void AnotherWorld::LogLegacy::setup (const LoggerConfiguration &configuration) {
    // Disable console logging if not explicitly asked from CLI interface.
    if (!configuration.isConsoleEnable()) {
        isSet = false;
    }
    severityLevel = configuration.getSeverityLevel();
}

void AnotherWorld::LogLegacy::log (SeverityLevels slvl,
                                   const char* format, ...) {
    if (isSet && slvl >= severityLevel) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(slvl, format, va);
        va_end(va);
    }
}

void AnotherWorld::LogLegacy::debug (ILogger::DebugSource cm,
                                     const char* format, ...) {
    if (isSet && cm && AnotherWorld::SeverityLevels::DEBUG >= severityLevel) {
        /// Buffer to stock command line.
        char buf[1024];
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        std::vsprintf(buf, format, va);
        va_end(va);
        std::printf("%s\n", buf);
        std::fflush(stdout);
    }
}

void AnotherWorld::LogLegacy::warning (const char* format, ...) {
    if (isSet && AnotherWorld::SeverityLevels::WARNING >= severityLevel) {
        /// Buffer to stock command line.
        char buf[1024];
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        std::vsprintf(buf, format, va);
        va_end(va);
        std::fprintf(stderr, "WARNING: %s!\n", buf);
    }
}

void AnotherWorld::LogLegacy::error (const char* format, ...) {
    if (isSet && AnotherWorld::SeverityLevels::ERROR >= severityLevel) {
        /// Buffer to stock command line.
        char buf[1024];
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        std::vsprintf(buf, format, va);
        va_end(va);
        std::fprintf(stderr, "ERROR: %s!\n", buf);
        exit(-1);
    }
}
