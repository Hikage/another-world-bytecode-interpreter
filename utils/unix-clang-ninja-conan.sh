#!/usr/bin/env sh
# -*- coding: utf-8 -*-

# Creates the build environment for Clang and Ninja, using Conan, on a Unix-like
# system.

# Clang C command.
CC=$(which clang)
# Clang C++ command.
CXX=$(which clang++)

conan install .. --settings compiler.libcxx="libstdc++11" -if ../build

cmake .. -DCMAKE_CXX_COMPILER=$CXX -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS_DEBUG="-g -pedantic -Wall -Werror -pipe" -DCMAKE_CXX_FLAGS_MINSIZEREL="-march=native -Os -DNDEBUG -pipe" -DCMAKE_CXX_FLAGS_RELEASE="-march=native -O3 -DNDEBUG -pipe" -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-march=native -O2 -g -pipe -fno-omit-frame-pointer" -DHTML=TRUE -G "Ninja" -B ../build -DUSE_CONAN=TRUE
