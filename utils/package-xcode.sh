#!/usr/bin/env sh
# -*- coding: utf-8 -*-

# Creates a build environment adapted for package making for Xcode.

cmake .. -DCMAKE_BUILD_TYPE=Relase -DCMAKE_CXX_FLAGS_DEBUG="-g -pedantic -Wall -Werror -pipe" -DCMAKE_CXX_FLAGS_MINSIZEREL="-march=native -Os -DNDEBUG -pipe" -DCMAKE_CXX_FLAGS_RELEASE="-march=native -O3 -DNDEBUG -pipe" -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-march=native -O2 -g -pipe -fno-omit-frame-pointer" -DDOCSET=TRUE -DMAN=TRUE -G "Xcode" -B ../build
